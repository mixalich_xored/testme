# Traceroute Topology Web Component


## Install
Via package manager:
```
npm install --save git@github.com:SpirentNetX/wc-topology.git[#<version>]
```

Via script tag (minified file should be part of the build):
```
<script src="index.min.js"></script>
```

## Usage examples

```javascript
import {renderTopology} from 'wc-topology';

renderTopology(data, document.getElementById('topology-container'));
```

Via script tag:
```html
<head>
    <script src="index.min.js"></script>
</head>
<body>
    <div id="topology-container"/>
</body>
<script>
    window.renderTopology(data, document.getElementById('topology-container'));
</script>
```

The `data` variable should satisfy CSL topology model, e.g.: https://cloudsure.spirentcom.com/o/api/task/traceroute/host/thor-CAL


## Development
- clone
- ```npm install```
- ```npm start```

Then visit http://127.0.0.1:8080

NPM scripts:
 - `npm start` starts a dev server
 - `npm test` runs tests
 - `npm run build` builds js and d.ts files
 - `npm run build:min` builds minified version to `dist/index.min.js` 
 - `npm run build:gallery` builds a gallery with examples to `dist-gallery/index.html`
