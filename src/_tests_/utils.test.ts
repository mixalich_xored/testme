import {getRandomColor, getUuid, average, createRandomGenerator} from '../utils';

describe('getRandomColor', () => {
    it('returns valid HEX format color', () => {
        const color = getRandomColor();
        expect(color).toMatch(/^#[0-9A-F]{6}$/i);
    });
});

describe('getUuid', () => {
    it('returns valid uuid', () => {
        const uuid = getUuid();
        expect(uuid).toMatch(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[0-3][0-9A-F]{3}-[0-9A-F]{12}$/i);
    });
});

describe('average', () => {
    it('returns 0 if array empty', () => {
        const arr: number[] = [];
        const avg = average(arr);
        expect(avg).toBe(0);
    });

    it('returns valid array average', () => {
        const arr: number[] = [-1, 2, -3, 4, -5, 6, -7, 8];
        const avg = average(arr);
        expect(avg).toBe(0.5);
    });
});

describe('createRandomGenerator', () => {
    it('return value from 0 to 1', () => {
        const gen = createRandomGenerator(0);
        const v = gen();

        expect(v).toBeGreaterThan(0);
        expect(v).toBeLessThan(1);
    });

    it('return different values for generators with different seed', () => {
        const gen0 = createRandomGenerator(0);
        const gen1 = createRandomGenerator(1);
        const v0 = gen0();
        const v1 = gen1();

        expect(v0).not.toBe(v1);
    });

    it('return same values for generators with same seed', () => {
        const gen0 = createRandomGenerator(0);
        const gen1 = createRandomGenerator(0);
        const v0 = gen0();
        const v1 = gen1();

        expect(v0).toBe(v1);
    });
});
