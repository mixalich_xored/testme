import {Graph} from 'graph';
import {average, createRandomGenerator} from 'utils';

const ROOT_ID = 'root';

const SEED = 142;

function isCrossing(edgeA: readonly [number, number], edgeB: readonly [number, number]) {
    if (edgeA[0] <= edgeB[0] && edgeA[1] <= edgeB[1]) {
        return false;
    }

    if (edgeA[0] >= edgeB[0] && edgeA[1] >= edgeB[1]) {
        return false;
    }

    return true;
}

function countCrossingNumber(layers: readonly string[][], graph: Graph<any>) {
    // O(E**2) now. It could be better but it doesn't matter because of input size.
    let sum = 0;

    for (let layerNum = 0; layerNum < layers.length - 1; layerNum++) {
        const firstLayer = layers[layerNum];
        const secondLayer = layers[layerNum + 1];

        const edges = firstLayer.flatMap((source, sourceIdx) => {
            return secondLayer
                .map((target, targetIdx) => [target, targetIdx] as const)
                .filter(([target]) => graph.hasEdge(source, target))
                .map(([, targetIdx]) => [sourceIdx, targetIdx] as const);
        });

        for (let i = 0; i < edges.length; i++) {
            const edgeA = edges[i];

            for (let j = i + 1; j < edges.length; j++) {
                const edgeB = edges[j];

                if (isCrossing(edgeA, edgeB)) {
                    sum++;
                }
            }
        }
    }

    return sum;
}

function sweepLayers(
    fixedLayer: readonly string[],
    mobileLayer: string[],
    graph: Graph<any>,
    isReverse: boolean,
    randomGenerator: () => number
) {
    const barycentricVals = new Map(
        mobileLayer.map(target => {
            const vals = fixedLayer
                .map((source, idx) => [source, idx] as const)
                .filter(([source]) => (isReverse ? graph.hasEdge(target, source) : graph.hasEdge(source, target)))
                .map(([, idx]) => idx + randomGenerator() * 0.0001);

            return [target, average(vals)] as const;
        })
    );

    mobileLayer.sort((a, b) => barycentricVals.get(a)! - barycentricVals.get(b)!);
}

export function optimizeDag(fullLayerDag: Graph<any>, forgivenessNumber: number): string[][] {
    const originalLayers = fullLayerDag.getLayers(ROOT_ID);

    let curForgivenessNumber = forgivenessNumber;
    let bestResult = Infinity;
    let bestLayer = originalLayers;
    let counter = 0;

    const randomGenerator = createRandomGenerator(SEED);
    const curLayer = originalLayers.map(layer => [...layer]);

    while (curForgivenessNumber > 0 || bestResult > 0) {
        counter++;

        const isReverse = counter % 2 === 0;

        const start = isReverse ? curLayer.length - 1 : 0;
        const end = isReverse ? 0 : curLayer.length - 1;
        const step = isReverse ? -1 : 1;

        for (let i = start; i !== end; i += step) {
            const fixedLayer = curLayer[i];
            const mobileLayer = curLayer[i + step];

            sweepLayers(fixedLayer, mobileLayer, fullLayerDag, isReverse, randomGenerator);
        }

        const crossingNumber = countCrossingNumber(curLayer, fullLayerDag);

        if (crossingNumber < bestResult) {
            bestResult = crossingNumber;
            bestLayer = curLayer.map(layer => [...layer]);
        } else {
            curForgivenessNumber--;
        }
    }

    console.log('bestResult', bestResult);

    return bestLayer;
}
