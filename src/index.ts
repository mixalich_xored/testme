import * as d3 from 'd3';

import {Edge, Graph as BaseGraph, mergeGraphs} from 'graph';
import {renderGraph, EdgeType, EdgeMeta} from 'render';
import {getUuid, isNotNil, last} from 'utils';
import {optimizeDag} from 'algo';

type Graph = BaseGraph<EdgeMeta>;

interface Node {
    isDrop: boolean;
    color: string;
    isPseudo: boolean;
    IP: string;
    id: string;
    name: string;
    children: Node[];
    asn?: {
        ip: string;
        announced: boolean;
        as_number?: number;
    };
}

interface City {
    ip: string;
}

interface Point {
    isDrop?: boolean;
    name?: string;
    city?: City;
    asn?: {
        ip: string;
        announced: boolean;
        as_number?: number;
    };
}

interface Result {
    trace?: Point[];
    time: number;
    meta?: {
        server?: string;
        destination?: {
            host: string;
        };
        ip?: {
            address?: string;
        };
    };
}

function colorize(node: Node, colorGen: () => string, mapping: Record<string, string> = {}, color = '') {
    const asnNumber = node?.asn?.as_number;
    let nodeColor = '';

    if (isNotNil(asnNumber)) {
        const freeColor = mapping[asnNumber] || colorGen();

        mapping[asnNumber] = freeColor;
        nodeColor = freeColor;
    } else {
        nodeColor = color || colorGen();
    }

    for (const child of node.children) {
        colorize(child, colorGen, mapping, nodeColor);
    }

    node.color = nodeColor;
}

function getPointId(point: Point): string {
    const id = point?.city?.ip;

    if (!id) {
        throw new Error(`Id expected, got ${id}`);
    }

    return id;
}

function hasId(point: Point) {
    return !!point?.city?.ip;
}

function pointToNode(point: Point): Node {
    const id = getPointId(point);

    const splitted = id.split('.');

    // const name = point.name ?? (splitted.length === 4 ? splitted.slice(0, 2).join('.') + '.x' : id); // TODO: change
    const name = point.name ?? id;

    return {
        isDrop: !!point.isDrop,
        asn: point.asn,
        color: 'gray',
        isPseudo: false,
        IP: id,
        id,
        name,
        children: [],
    };
}

function collapseLossNodes(trace: readonly Point[], traceId: string, meta?: Result['meta']): Point[] {
    const pureTrace: Point[] = [
        {
            city: {
                ip: ROOT_ID,
            },
        },
    ];

    let lossCounter = 0;

    for (let i = 0; i < trace.length; i++) {
        const point = trace[i];

        if (hasId(point)) {
            lossCounter = 0;
            pureTrace.push(point);
            continue;
        }

        lossCounter++;

        const nextPoint = trace[i + 1];

        if (nextPoint && !hasId(nextPoint)) {
            continue;
        }

        const lastActiveNode = last(pureTrace);

        const prevActiveNodeId = lastActiveNode ? getPointId(lastActiveNode) : traceId;
        const nextActiveNodeId = nextPoint ? getPointId(nextPoint) : 'end';

        pureTrace.push({
            isDrop: true,
            name: `${lossCounter}`,
            city: {
                ip: `${prevActiveNodeId}-${lossCounter}-${nextActiveNodeId}`,
            },
        });
    }

    const server = meta?.server;

    if (server) {
        const lastPoint = last(pureTrace);

        if (lastPoint && meta?.ip?.address === getPointId(lastPoint)) {
            pureTrace.pop();
        }

        pureTrace.push({
            city: {
                ip: server,
            },
        });
    }

    return pureTrace;
}

function getTraceVertices(trace: readonly Point[]): string[] {
    return trace.map(point => getPointId(point));
}

function getTraceEdges(trace: readonly Point[], traceId: string) {
    const edges: Edge<EdgeMeta>[] = [];

    for (let i = 0; i < trace.length - 1; i++) {
        edges.push({
            source: getPointId(trace[i]),
            target: getPointId(trace[i + 1]),
            meta: {traceId},
        });
    }

    return edges;
}

function mergeDags(graphs: readonly Graph[]): Graph[] {
    if (graphs.length <= 1) {
        return [...graphs];
    }

    const [head, ...rest] = graphs;
    let dag = head;
    let unmergedGraphs: Graph[] = [];

    for (const graph of rest) {
        const mergedGraph = mergeGraphs(dag, graph);

        if (mergedGraph.isCyclic()) {
            unmergedGraphs.push(graph);

            continue;
        }

        dag = mergedGraph;
    }

    if (unmergedGraphs.length === 0) {
        return [dag];
    }

    return [dag, ...mergeDags(unmergedGraphs)];
}

const ROOT_ID = 'root';

function addPseudoVertices(dag: Graph): Graph {
    const edges = dag.getEdges();

    const depths = dag.getNodeDepths(ROOT_ID);

    const getPseudoNodeId = () => `pseudo-${getUuid()}`;

    const pseudoVertices: string[] = [];
    const pseudoEdges: Edge<EdgeMeta>[] = [];

    for (const edge of edges) {
        const sourceLevel = depths.get(edge.source)!;
        const targetLevel = depths.get(edge.target)!;

        const delta = targetLevel - sourceLevel;

        if (delta < 1) {
            throw new Error(`Wrong delta value ${delta}`);
        }

        if (delta === 1) {
            continue;
        }

        const edgePseudoVertices: string[] = [];
        const edgePseudoEdges: Edge<EdgeMeta>[] = [];

        for (let i = 0; i < delta; i++) {
            edgePseudoVertices.push(getPseudoNodeId());
        }

        const pseudoPath = [edge.source, ...edgePseudoVertices, edge.target];

        for (let i = 0; i < pseudoPath.length - 1; i++) {
            edgePseudoEdges.push({
                source: pseudoPath[i],
                target: pseudoPath[i + 1],
                meta: {traceId: 'none'},
            });
        }

        pseudoVertices.push(...edgePseudoVertices);
        pseudoEdges.push(...edgePseudoEdges);
    }

    const pseudoGraph = new BaseGraph(pseudoVertices, pseudoEdges);
    const fullLayerDag = mergeGraphs(dag, pseudoGraph);

    return fullLayerDag;
}

function prepareDagForRender(dag: Graph, points: ReadonlyMap<string, Point>, renderParams: RenderParams) {
    const fullLayerGraph = addPseudoVertices(dag);

    const optimizedLayers: Node[][] = optimizeDag(fullLayerGraph, renderParams.forgivenessNumber).map(layer => {
        return layer.map((id): Node => {
            const point = points.get(id);

            return point
                ? pointToNode(point)
                : {
                      color: 'red',
                      isDrop: false,
                      isPseudo: true,
                      IP: '',
                      id,
                      name: 'Pseudo',
                      children: [],
                  };
        });
    });

    for (let i = 0; i < optimizedLayers.length - 1; i++) {
        const layer = optimizedLayers[i];
        const nextLayer = optimizedLayers[i + 1];

        inner: for (const nextLayerNode of nextLayer) {
            for (const node of layer) {
                if (fullLayerGraph.hasEdge(node.id, nextLayerNode.id)) {
                    node.children.push(nextLayerNode);
                    continue inner;
                }
            }

            throw new Error("Internal error. Doesn't expect to be here");
        }
    }

    let i = 0;
    const getNextColor = () => d3.schemeSet1[i++];

    const rootNode = optimizedLayers[0][0];

    colorize(rootNode, getNextColor, {}, 'gray');

    return [rootNode, dag.getEdges()] as const;
}

function getData(results: readonly Result[], renderParams: RenderParams): HTMLDivElement[] {
    const traces = results
        .filter(({trace}) => !!trace)
        .map(({trace, meta}, i) => collapseLossNodes(trace!, `${i}`, meta));

    const points = new Map(traces.flatMap(p => p).map(p => [getPointId(p), p] as const));

    const graphs = traces.map((trace, i) => {
        const vertices = getTraceVertices(trace);
        const edges = getTraceEdges(trace, `trace-${i}`);

        return new BaseGraph(vertices, edges);
    });

    if (graphs.length === 0) {
        throw Error('Empty trace error');
    }

    if (graphs.some(graph => graph.isCyclic())) {
        throw Error('Trace is cyclic!');
    }

    const dags = mergeDags(graphs);

    return dags.map(dag => renderDag(...prepareDagForRender(dag, points, renderParams), renderParams)).filter(isNotNil);
}

function renderDag(root: Node, edges: readonly Edge<EdgeMeta>[], renderParams: RenderParams) {
    return renderGraph(root, edges, renderParams);
}

export interface RenderParams {
    forgivenessNumber: number;
    edgeType: EdgeType;
}

const DEFAULT_PARAMS: RenderParams = {
    forgivenessNumber: 20,
    edgeType: 'bezier',
};

export default function renderTopology(result: readonly Result[], el: Element, params: Partial<RenderParams> = {}) {
    const renderParams = {...DEFAULT_PARAMS, ...params};

    for (const div of getData(result, renderParams)) {
        el.appendChild(div);
    }
}

if (process.env.IS_MINIFIED) {
    (window as any).renderTopology = renderTopology;
}
