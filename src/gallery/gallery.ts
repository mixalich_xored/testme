import mockData from './examples/mock.json';
import mockData2 from './examples/mock2.json';
import mockData3 from './examples/mock3.json';
import graphWithCycle from './examples/graphWithCycle.json';
import renderTopology from '../index';

enum DataMode {
    READONLY = 'readonly',
    HIDDEN = 'hidden',
    EDITABLE = 'editable',
    UNKNOWN = 'unknown',
}

enum DisplayMode {
    GRAPHS = 'graphs',
    GRAPH = 'graph',
}

interface City {
    ip: string;
}
interface Point {
    city?: City, 
    ttl?: number,
    asn?: {
        ip: string;
        announced: boolean;
        as_number?: number;
    };
}

interface Result {
    trace: Point[];
    time: number;
}

interface GraphInputData {
    result: Result[],
    task: string,
    time: number,
}

type GraphSpecialQuery = "all"; //todo: add random into another branch

let currentSelectedLi: HTMLElement;
const ul = document.getElementById('dynamic-list')!;
const graphHolder = document.getElementById('graph')!;
const inputTextArea = <HTMLInputElement>document.getElementById('text-area')!;
const updateBtn = <HTMLButtonElement>document.getElementById('update-btn')!;
const showAllBtn = <HTMLButtonElement>document.getElementById('show-all-graphs-btn')!;

let currentGraph: GraphData;

updateBtn.onclick = () => handleUpdateOnClick();
showAllBtn.onclick = () => handleShowAllOnClick();

interface GraphData {
    graph: GraphInputData;
    id: number;
    label: string;
}

const addListItem = (graphData: GraphData) => {
    const li = document.createElement('li');
    li.setAttribute('id', `data-${graphData.id}`);
    li.appendChild(document.createTextNode(`${graphData.label}`));

    li.onclick = () => {
        currentGraph = graphData;
        updateGraphUrl(graphData.id);
    };
    ul.appendChild(li);
};

const handleUpdateOnClick = () => {
    const newDataStr = inputTextArea.value;

    let newData;
    try {
        newData = JSON.parse(newDataStr);
    } catch (error) {
        alert(`Error occurred during data parsing: ${error.message}`);
        return;
    }
    try {
        const updatedGraphData = { ...currentGraph, graph: newData };
        drawGraph(updatedGraphData);
        currentGraph = updatedGraphData;
    } catch (error) {
        alert(`Could not draw graph: ${error.message}. Please fix input data.`);
        return;
    }
};

const handleShowAllOnClick = () => {
    updateGraphUrl("all");
}

const drawGraph = (graphData: GraphData) => {
    graphHolder.innerHTML = '';

    graphHolder.appendChild(createGraphElement(graphData));

    inputTextArea.value = '';
    displayJSON(graphData.graph);
};

const createGraphElement = (graphData: GraphData): HTMLDivElement => {
    const graphDataContainer = document.createElement('div');

    const graphName = document.createElement('span');
    graphName.textContent = graphData.label;
    graphName.style.fontSize = "20px";
    graphDataContainer.appendChild(graphName);

    const graphPlace = document.createElement('div');
    graphPlace.style.marginTop = "30px";

    renderTopology(graphData.graph.result, graphPlace);

    graphDataContainer.appendChild(graphPlace);

    graphDataContainer.style.border="1px solid black";
    graphDataContainer.style.padding = "5px";
    graphDataContainer.style.marginBottom = "5px";

    return graphDataContainer;
}

const makeSelectedListItemBold = (id: number) => {
    if (currentSelectedLi) {
        currentSelectedLi.style.fontWeight = '';
    }

    const selectedLi = document.getElementById(`data-${id}`)!;
    selectedLi.style.fontWeight = '900';
    currentSelectedLi = selectedLi;
};

const updateGraphUrl = (nextId: number | GraphSpecialQuery) => {
    const urlParams = new URLSearchParams(window.location.search);

    const currentId = getGraphIdFromURL();
    if (nextId !== currentId) {
        urlParams.set('graph', nextId.toString());
        window.location.search = urlParams.toString();
    }
};

const displayJSON = (date: GraphInputData) => {
    inputTextArea.value += JSON.stringify(date, undefined, 4);
};

const hideElement = (elementId: string) => {
    const elementToHide = document.getElementById(elementId)!;
    elementToHide.style.display = 'none';
};

const getGraphIdFromURL = (): number | GraphSpecialQuery => {
    const urlParams = new URLSearchParams(window.location.search);
    const graph = urlParams.get('graph');

    if (graph === "all") {
        return graph;
    }

    if (graph && Number.isInteger(+graph)) {
        return +graph;
    } else {
        return 0;
    }
};

const initGraph = (displayMode: DisplayMode, graphsData: GraphData[]) => {
    if (displayMode === DisplayMode.GRAPH) {
        const id = getGraphIdFromURL();

        const el = graphsData.find(e => e.id === id);

        if (el && Number.isInteger(+id)) {
            drawGraph(el);
            currentGraph = el;
            makeSelectedListItemBold(+id);
        } else {
            alert(`Invalid graph id: ${id}`);
        }
    } else if (displayMode === DisplayMode.GRAPHS) {
        drawAllGraphs(graphsData, graphHolder);
    }
};

const getDisplayGraphMode = (): DisplayMode => {
    const urlParams = new URLSearchParams(window.location.search);
    const graph = urlParams.get('graph');

    if (graph && graph.toLowerCase() === 'all') {
        return DisplayMode.GRAPHS;
    }

    if (!graph || !Number.isInteger(+graph)) {
        urlParams.set('graph', '0');
        window.location.search = urlParams.toString();
    }

    return DisplayMode.GRAPH;
};

const getDataMode = (displayMode: DisplayMode): DataMode => {
    const urlParams = new URLSearchParams(window.location.search);
    const mode = urlParams.get('mode') ?? 'unknown';

    let dateMode: DataMode;
    if (Object.values(DataMode).some((col: string) => col === mode)) {
        dateMode = <DataMode>mode;
    } else {
        alert('Invalid mode');
        dateMode = DataMode.UNKNOWN;
    }

    if (displayMode === DisplayMode.GRAPHS && dateMode === DataMode.EDITABLE) {
        alert('Editable mode is not enabled in current display mode.');
        return DataMode.HIDDEN;
    }

    if (dateMode === DataMode.UNKNOWN) {
        switch (displayMode) {
            case DisplayMode.GRAPH:
                return DataMode.EDITABLE;
            case DisplayMode.GRAPHS:
                return DataMode.HIDDEN;
        }
    }

    return dateMode;
};

const getIsOnlyGraph = (): boolean => {
    const urlParams = new URLSearchParams(window.location.search);
    const mode = urlParams.get('onlyGraph');

    if (!mode) {
        return false;
    }

    return mode.toLowerCase() === 'true';
};

const drawAllGraphs = (graphsData: GraphData[], element: HTMLElement) => {
    element.innerHTML = '';
    graphsData.forEach(data => {
        inputTextArea.value += `Graph ${data.id}: \n`;

        const graphElement = createGraphElement(data);
        element.appendChild(graphElement);

        displayJSON(data.graph);
        inputTextArea.value += `\n`;
    });
};

const handleInputAreaMode = (dateMode: DataMode) => {
    if (dateMode === DataMode.HIDDEN) {
        hideElement('input-area');
    } else if (dateMode === DataMode.READONLY) {
        inputTextArea.readOnly = true;
        updateBtn.disabled = true;
    }
};

const handleGraphList = () => {
    if (getIsOnlyGraph()) {
        hideElement('list-area');
    }
};

const mockedDataArr = [
    { graph: mockData, id: 0, label: 'Small single source graph' },
    { graph: mockData2, id: 1, label: 'Large single source graph' },
    { graph: mockData3, id: 2, label: 'Single target graph' },
    { graph: graphWithCycle, id: 3, label: 'Graph with cycle' },
];

mockedDataArr.forEach(data => addListItem(data));

const displayMode = getDisplayGraphMode();
initGraph(displayMode, mockedDataArr);

const dateMode = getDataMode(displayMode);
handleInputAreaMode(dateMode);
handleGraphList();
