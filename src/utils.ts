// TODO: remove if we don't need it
export function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

export function getUuid(a: string = ''): string {
    return a
        ? ((Number(a) ^ (Math.random() * 16)) >> (Number(a) / 4)).toString(16)
        : `${1e7}-${1e3}-${4e3}-${8e3}-${1e11}`.replace(/[018]/g, getUuid);
}

export function average(arr: readonly number[]): number {
    if (arr.length === 0) {
        return 0;
    }

    let sum = 0;

    for (const num of arr) {
        sum += num;
    }

    return sum / arr.length;
}
export function createRandomGenerator(seed: number) {
    return () => {
        let t = (seed += 0x6d2b79f5);

        t = Math.imul(t ^ (t >>> 15), t | 1);
        t ^= t + Math.imul(t ^ (t >>> 7), t | 61);

        return ((t ^ (t >>> 14)) >>> 0) / 4294967296;
    };
}

export function isNotNil<T>(some: T | null | undefined): some is T {
    return some !== null && some !== undefined;
}

export function last<T>(arr: readonly T[]): T | undefined {
    return arr[arr.length - 1];
}
