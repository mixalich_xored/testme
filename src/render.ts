import * as d3 from 'd3';
import {HierarchyPointNode} from 'd3';

import {Edge} from 'graph';

const WIDTH = 1900;
const DEFAULT_LINK_COLOR = '#555';

export interface EdgeMeta {
    traceId: string;
}

interface Node {
    isDrop: boolean;
    color: string;
    IP: string;
    id: string;
    name: string;
    isPseudo: boolean;
    children: Node[];
    asn?: {
        ip: string;
        announced: boolean;
        as_number?: number;
    };
}

type NodeHolder = d3.HierarchyPointNode<Node>;
export type EdgeType = 'straight' | 'bezier';

function renderStraightEdge(s: {source: NodeHolder; target: NodeHolder}) {
    const l = d3.line()([
        [s.target.y, s.target.x],
        [s.source.y, s.source.x],
    ]);

    return l;
}

function drawEdges(
    g: d3.Selection<SVGGElement, undefined, null, undefined>,
    edges: readonly Edge<EdgeMeta>[],
    descendants: NodeHolder[],
    edgeType: EdgeType
) {
    const treeNodes = new Map<string, HierarchyPointNode<Node>>();

    for (const d of descendants) {
        treeNodes.set(d.data.id, d);
    }

    const links = edges
        .filter(edge => edge.source !== 'root')
        .map(edge => {
            const source = treeNodes.get(edge.source)!;
            const target = treeNodes.get(edge.target)!;

            return {
                source,
                target,
                color: '#3f9cf2',
                traceId: edge.meta.traceId,
            };
        });

    const DEFAULT_STROKE = 1.5;

    const renderBezierEdge = d3
        .linkHorizontal()
        .x((d: any) => d.y)
        .y((d: any) => d.x) as any;

    g.append('g')
        .attr('fill', 'none')
        .attr('stroke', DEFAULT_LINK_COLOR)
        .attr('stroke-opacity', 0.4)
        .attr('stroke-width', DEFAULT_STROKE)
        .attr('cursor', 'pointer')
        .selectAll('path')
        .data(links)
        .join('path')
        .attr('d', edgeType === 'straight' ? renderStraightEdge : renderBezierEdge)
        .on('mouseover', (_, link) => {
            g.selectAll('path')
                .filter((d: any) => {
                    return d.traceId === link.traceId;
                })
                .attr('stroke', link.color)
                .attr('stroke-width', 8);
        })
        .on('mouseout', () => {
            g.selectAll('path').attr('stroke', DEFAULT_LINK_COLOR).attr('stroke-width', DEFAULT_STROKE);
        });
}

function renderTooltipContent(node: Node) {
    const div = document.createElement('div');

    if (node.isDrop) {
        div.innerText = 'no response';
    } else {
        div.innerText = `IP: ${node.IP}`;

        if (node.asn?.ip) {
            div.innerText += `\nASN IP: ${node.asn.ip}`;
        }
    }

    return div;
}

function addTooltip(node: d3.Selection<SVGCircleElement, NodeHolder, SVGGElement, undefined>) {
    const tooltip = document.createElement('div');

    document.body.appendChild(tooltip);

    tooltip.style.setProperty('position', 'absolute');
    tooltip.style.setProperty('z-index', '2');
    tooltip.style.setProperty('border', '1px solid #000');
    tooltip.style.setProperty('visibility', 'hidden');
    tooltip.style.setProperty('background-color', '#fff');
    tooltip.style.setProperty('padding', '10px');

    tooltip.textContent = 'Tooltip text';

    const CLOSE_TIMEOUT = 500;
    let timerId: any = null;
    let tooltipId: string | null = null;

    const handleShow = (event: any, node: Node) => {
        if (timerId) {
            clearTimeout(timerId);
        }

        if (tooltipId === node.id) {
            return;
        }

        tooltipId = node.id;

        tooltip.innerHTML = '';

        const step = 5;

        const content = renderTooltipContent(node);

        const x = Math.ceil(event.pageX / step) * step;
        const y = Math.ceil(event.pageY / step) * step;

        tooltip.style.setProperty('top', y + 'px');
        tooltip.style.setProperty('left', x + 'px');
        tooltip.appendChild(content);
        tooltip.style.setProperty('visibility', 'visible');
    };

    const handleClose = () => {
        tooltipId = null;
        tooltip.style.setProperty('visibility', 'hidden');
        tooltip.innerHTML = '';
    };

    const handleMouseOut = () => {
        if (timerId) {
            clearTimeout(timerId);
        }

        timerId = setTimeout(() => {
            handleClose();
        }, CLOSE_TIMEOUT);
    };

    node.on('mouseenter', (event, arg) => {
        handleShow(event, arg.data);
    }).on('mouseleave', handleMouseOut);

    tooltip.onmouseenter = () => {
        if (timerId) {
            clearTimeout(timerId);
        }
    };

    tooltip.onmouseleave = handleMouseOut;
}

export function renderGraph(data: Node, edges: readonly Edge<EdgeMeta>[], params: {edgeType: EdgeType}) {
    const nodeSizeX = 35;
    const nodeSizeY = 80;

    const rootElement = document.createElement('div');

    const treeRoot = d3.tree<Node>().nodeSize([nodeSizeX, nodeSizeY])(d3.hierarchy<Node>(data));

    const descendants = treeRoot.descendants();
    const xVals = descendants.map(v => v.x);

    const x0 = Math.min(...xVals);
    const x1 = Math.max(...xVals);

    const dx = 10;
    const dy = WIDTH / (treeRoot.height + 1);

    const svg = d3.create('svg').attr('viewBox', [0, 0, WIDTH, x1 - x0 + nodeSizeX * 2 + 100] as any);

    const g = svg
        .append('g')
        .attr('font-family', 'sans-serif')
        .attr('font-size', 10)
        .attr('transform', `translate(${dy / 3},${dx - x0})`);

    drawEdges(g, edges, descendants, params.edgeType);

    const node = g
        .append('g')
        .attr('stroke-linejoin', 'round')
        .attr('stroke-width', 3)
        .selectAll('g')
        .data(descendants.filter(el => el.data.id !== 'root' && !el.data.isPseudo))
        .join('g')
        .attr('transform', d => `translate(${d.y},${d.x})`);

    // if (v.latency > 400 * 1e6) this.status.latency.attr("fill", "red")
    // else if (v.latency > 200 * 1e6) this.status.latency.attr("fill", "orange")
    // else this.status.latency.attr("fill", "lime")

    const RADIUS = 8;
    const circle = node
        .append('circle')
        .attr('stroke-width', 1)
        .attr('stroke', '#999')
        .attr('cursor', 'pointer')
        .attr('fill', d => (d.data.isDrop ? '#000' : d.data.color))
        .attr('r', d => (d.data.isDrop ? RADIUS / 2 : RADIUS));

    addTooltip(circle);

    node.append('text')
        .attr('x', d => (d.data.isDrop ? 0 : d.children ? -6 : 6))
        .attr('y', 2 * RADIUS + 2)
        .attr('text-anchor', 'middle')
        .attr('font-size', '11px')
        .text(d => d.data.name)
        .clone(true)
        .lower()
        .attr('stroke', 'white');

    const svgNode = svg.node();

    if (!svgNode) {
        return null;
    }

    rootElement.appendChild(svgNode);

    return rootElement;
}
