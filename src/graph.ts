export interface Edge<T> {
    source: string;
    target: string;
    meta: T;
}

export class Graph<E> {
    public vertices: readonly string[];
    public edges: ReadonlyMap<string, ReadonlySet<string>>;
    private _isCyclic: boolean | null = null;
    private _edges: readonly Edge<E>[];

    constructor(vertices: readonly string[], edges: readonly Edge<E>[]) {
        const edgesMap = new Map<string, Set<string>>();
        this._edges = edges.filter(({source, target}) => source !== target);

        for (const {source, target} of edges) {
            if (source === target) {
                continue;
            }

            const set = edgesMap.get(source) || new Set();

            set.add(target);

            if (!edgesMap.has(source)) {
                edgesMap.set(source, set);
            }
        }

        // TODO: remove duplicate edges and save traces id in the edge
        // this._edges = [...edgesMap.entries()].flatMap(([source, targets]) =>
        //     [...targets.values()].map(target => ({source, target, color: ''}))
        // );

        this.vertices = [...new Set(vertices)];
        this.edges = edgesMap;
    }

    isCyclic() {
        if (this._isCyclic !== null) {
            return this._isCyclic;
        }

        enum Color {
            Gray,
            Black,
        }

        const colors = new Map<string, Color>();
        let isCyclic = false;

        const dfs = (v: string) => {
            if (isCyclic) {
                return;
            }

            const color = colors.get(v);

            if (color === Color.Gray) {
                isCyclic = true;
                return;
            }

            if (color === Color.Black) {
                return;
            }

            colors.set(v, Color.Gray);

            for (const ch of this.edges.get(v) || new Set()) {
                dfs(ch);
            }

            colors.set(v, Color.Black);
        };

        for (const v of this.vertices) {
            dfs(v);
        }

        this._isCyclic = isCyclic;

        return isCyclic;
    }

    hasEdge(source: string, target: string) {
        return !!this.edges.get(source)?.has(target);
    }

    getEdges() {
        return this._edges;
    }

    getNodeDepths(root: string): Map<string, number> {
        if (this.isCyclic()) {
            throw Error("Can't get layers from cyclic graph");
        }

        const depths = new Map<string, number>();

        const dfs = (v: string, level: number) => {
            depths.set(v, Math.max(level, depths.get(v) || level));

            for (const ch of this.edges.get(v) || new Set()) {
                dfs(ch, level + 1);
            }
        };

        dfs(root, 0);

        return depths;
    }

    getLayers(root: string): string[][] {
        const depths = this.getNodeDepths(root); // TODO: optimize
        const layers: string[][] = [];

        for (const v of this.vertices) {
            const vLevel = depths.get(v);

            if (vLevel === undefined) {
                throw new Error(`Unknown depth of node ${v}`);
            }

            layers[vLevel] ||= [];
            layers[vLevel].push(v);
        }

        return layers;
    }
}

export function mergeGraphs<T>(graphA: Graph<T>, graphB: Graph<T>): Graph<T> {
    const vertices = [...graphA.vertices, ...graphB.vertices];
    const edges = [...graphA.getEdges(), ...graphB.getEdges()];

    return new Graph(vertices, edges);
}
