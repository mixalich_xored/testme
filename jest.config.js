module.exports = {
  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'node',
  coverageReporters: ['html'],
  collectCoverageFrom: ['src/**/*.{js,ts}', '!src/**/_mocks_/**', '!src/**/_stories_/**'],
  roots: ['./src/'],
  testMatch: ['**/_tests_/**/*test.{js,ts}'],
  modulePaths: ['./src/'],
  moduleFileExtensions: ['ts', 'js', 'json'],
};
