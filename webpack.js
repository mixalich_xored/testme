const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

const root = path.resolve(__dirname, './');

const PATHS = {
    root,
    dist: path.resolve(root, 'dist'),
    gallery_dist: path.resolve(root, 'dist-gallery'),
    appSrc: path.resolve(root, 'src'),
};

module.exports = function config(env, options) {
    const isDevelopment = options.mode === 'development';

    return {
        entry: isDevelopment ? './src/gallery/gallery.ts' : './src/index.ts',
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: {
                        loader: 'ts-loader',
                        options: {
                            onlyCompileBundledFiles: true,
                        },
                    },
                },
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
            plugins: [new TsconfigPathsPlugin({configFile: './tsconfig.json'})],
        },
        output: {
            filename: 'index.min.js',
            path: isDevelopment ? PATHS.gallery_dist : PATHS.dist,
            publicPath: '/',
        },
        devServer: {
            historyApiFallback: true,
            port: 8282,
            contentBase: PATHS.gallery_dist,
        },
        devtool: isDevelopment ? 'source-map' : false,
        optimization: {
            minimize: true,
        },
        plugins: [
            ...(isDevelopment
                ? [
                      new HtmlWebpackPlugin({
                          template: './src/gallery/index.html',
                          scriptLoading: 'blocking',
                      }),
                  ]
                : []),
            new webpack.DefinePlugin({
                'process.env.IS_MINIFIED': JSON.stringify(true),
            }),
        ],
    };
};
